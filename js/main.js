
let checkValue = (someValue) => {
    while (someValue == null || isNaN(Number(someValue)) || Number(someValue) <= 0 || Number.isInteger(Number(someValue)) === false) {
        if (someValue == null) {
            someValue = prompt("Введите число повторно");
            continue;
        }
        if (isNaN(Number(someValue))) {
            someValue = prompt("Введите число!");
            continue;
        } if (Number(someValue) <= 0) {
            someValue = prompt("Введите число больше 0");
            continue;
        }
        if (Number.isInteger(Number(someValue)) === false){
            someValue = prompt("Введите целое число");
        }
    }
    return Number(someValue);
}

let easterDate = (year) => {
    let a = year % 19;
    let b = year % 4;
    let c = year % 7;
    let d = (19 * a + 15) % 30;
    let e = (2 * b + 4 * c + 6 * d + 6) % 7;
    let f = d + e;
    let easterDate;

    if (f > 9) {
        let date = Number(f - 9 + 13);
        if (date >=31){
          date = date - 31;
          easterDate = date + " мая";
        }

        else{
            easterDate = date + " апреля";
        }
    }
    else {
        date = Number(f + 22+ 13);
        if (date >=31){
            date = date - 31;
            easterDate = date + " апреля";
          }
        else{
            easterDate = date + " марта";
        }
    }
    return easterDate;
};

let easterDateCheck = () => {
    alert("Дата пасхи: " + easterDate(checkValue(prompt("Введите год"))));
    document.getElementById("happyEaster").style.display = 'block';
};

